use std::{
    alloc::{self, Layout},
    marker, ptr,
};

#[derive(Debug, PartialEq, Eq)]
pub enum Error {
    EmptyBuffer,
    FullBuffer,
}

pub struct CircularBuffer<T> {
    pointer: ptr::NonNull<T>,
    capacity: usize,
    write_index: usize,
    read_index: usize,
    phantom: std::marker::PhantomData<T>,
    size: usize,
}

impl<T> CircularBuffer<T> {
    pub fn new(capacity: usize) -> Self {
        let layout: Layout = Layout::array::<T>(capacity).unwrap();
        let pointer: *mut u8 = unsafe { alloc::alloc(layout) };
   
        Self {
            pointer: match ptr::NonNull::new(pointer as *mut T) {
                Some(pointer) => pointer,
                None => alloc::handle_alloc_error(layout),
            },
            capacity,
            phantom: marker::PhantomData,
            write_index: 0,
            read_index: 0,
            size: 0,
        }
    }

    pub fn write(&mut self, element: T) -> Result<(), Error> {

        if self.is_full() {
            return Err(Error::FullBuffer);
        }
        
        unsafe {
            ptr::write(self.pointer.as_ptr().add(self.write_index), element);
        }
        
        self.write_index = (self.write_index + 1) % self.capacity;

        self.size += 1;

        Ok(())
    }

    pub fn read(&mut self) -> Result<T, Error> {
        if self.is_empty() {
            return Err(Error::EmptyBuffer);
        }

        let index_value: T = unsafe { ptr::read(self.pointer.as_ptr().add(self.read_index)) };
        
        self.read_index = (self.read_index + 1) % self.capacity;
        
        self.size -= 1;

        Ok(index_value)
    }

    pub fn clear(&mut self) {
        for index in 0..self.size {
            unsafe { ptr::drop_in_place(self.pointer.as_ptr().add(index)) };
        }
        self.read_index = 0;
        self.write_index = 0;
        self.size = 0;
    }

    pub fn overwrite(&mut self, element: T) {
        if !self.is_full() {
            let _ = self.write(element);
            return;
        }
 
        unsafe {
            ptr::drop_in_place(self.pointer.as_ptr().add(self.read_index));
            ptr::write(self.pointer.as_ptr().add(self.read_index), element);
        }

        self.read_index = (self.read_index + 1) % self.capacity;
    }

    pub fn is_full(&self) -> bool {
        self.size == self.capacity
    }

    pub fn is_empty(&self) -> bool {
        self.size == 0
    }
}
fn main() {
}

#[cfg(test)]
mod test {
    use super::*;
    use std::rc::Rc;
    use {CircularBuffer, Error};

    #[test]
    fn error_on_read_empty_buffer() {
        let mut buffer = CircularBuffer::<char>::new(1);
        assert_eq!(Err(Error::EmptyBuffer), buffer.read());
    }

    #[test]
    fn can_read_item_just_written() {
        let mut buffer = CircularBuffer::new(1);
        assert!(buffer.write('1').is_ok());
        assert_eq!(Ok('1'), buffer.read());
    }

    #[test]
    fn each_item_may_only_be_read_once() {
        let mut buffer = CircularBuffer::new(1);
        assert!(buffer.write('1').is_ok());
        assert_eq!(Ok('1'), buffer.read());
        assert_eq!(Err(Error::EmptyBuffer), buffer.read());
    }

    #[test]
    fn items_are_read_in_the_order_they_are_written() {
        let mut buffer = CircularBuffer::new(2);
        assert!(buffer.write('1').is_ok());
        assert!(buffer.write('2').is_ok());
        assert_eq!(Ok('1'), buffer.read());
        assert_eq!(Ok('2'), buffer.read());
        assert_eq!(Err(Error::EmptyBuffer), buffer.read());
    }

    #[test]
    fn full_buffer_cant_be_written_to() {
        let mut buffer = CircularBuffer::new(1);
        assert!(buffer.write('1').is_ok());
        assert_eq!(Err(Error::FullBuffer), buffer.write('2'));
    }

    #[test]
    fn read_frees_up_capacity_for_another_write() {
        let mut buffer = CircularBuffer::new(1);
        assert!(buffer.write('1').is_ok());
        assert_eq!(Ok('1'), buffer.read());
        assert!(buffer.write('2').is_ok());
        assert_eq!(Ok('2'), buffer.read());
    }

    #[test]
    fn read_position_is_maintained_even_across_multiple_writes() {
        let mut buffer = CircularBuffer::new(3);
        assert!(buffer.write('1').is_ok());
        assert!(buffer.write('2').is_ok());
        assert_eq!(Ok('1'), buffer.read());
        assert!(buffer.write('3').is_ok());
        assert_eq!(Ok('2'), buffer.read());
        assert_eq!(Ok('3'), buffer.read());
    }

    #[test]
    fn items_cleared_out_of_buffer_cant_be_read() {
        let mut buffer = CircularBuffer::new(1);
        assert!(buffer.write('1').is_ok());
        buffer.clear();
        assert_eq!(Err(Error::EmptyBuffer), buffer.read());
    }

    #[test]
    fn clear_frees_up_capacity_for_another_write() {
        let mut buffer = CircularBuffer::new(1);
        assert!(buffer.write('1').is_ok());
        buffer.clear();
        assert!(buffer.write('2').is_ok());
        assert_eq!(Ok('2'), buffer.read());
    }

    #[test]
    fn clear_does_nothing_on_empty_buffer() {
        let mut buffer = CircularBuffer::new(1);
        buffer.clear();
        assert!(buffer.write('1').is_ok());
        assert_eq!(Ok('1'), buffer.read());
    }

    #[test]
    fn clear_actually_frees_up_its_elements() {
        let mut buffer = CircularBuffer::new(1);
        let element = Rc::new(());
        assert!(buffer.write(Rc::clone(&element)).is_ok());
        assert_eq!(Rc::strong_count(&element), 2);
        buffer.clear();
        assert_eq!(Rc::strong_count(&element), 1);
    }

    #[test]
    fn overwrite_acts_like_write_on_non_full_buffer() {
        let mut buffer = CircularBuffer::new(2);
        assert!(buffer.write('1').is_ok());
        buffer.overwrite('2');
        assert_eq!(Ok('1'), buffer.read());
        assert_eq!(Ok('2'), buffer.read());
        assert_eq!(Err(Error::EmptyBuffer), buffer.read());
    }

    #[test]
    fn overwrite_replaces_the_oldest_item_on_full_buffer() {
        let mut buffer = CircularBuffer::new(2);
        assert!(buffer.write('1').is_ok());
        assert!(buffer.write('2').is_ok());
        buffer.overwrite('A');
        assert_eq!(Ok('2'), buffer.read());
        assert_eq!(Ok('A'), buffer.read());
    }

    #[test]
    fn overwrite_replaces_the_oldest_item_remaining_in_buffer_following_a_read() {
        let mut buffer = CircularBuffer::new(3);
        assert!(buffer.write('1').is_ok());
        assert!(buffer.write('2').is_ok());
        assert!(buffer.write('3').is_ok());
        assert_eq!(Ok('1'), buffer.read());
        assert!(buffer.write('4').is_ok());
        buffer.overwrite('5');
        assert_eq!(Ok('3'), buffer.read());
        assert_eq!(Ok('4'), buffer.read());
        assert_eq!(Ok('5'), buffer.read());
    }

    #[test]
    fn integer_buffer() {
        let mut buffer = CircularBuffer::new(2);
        assert!(buffer.write(1).is_ok());
        assert!(buffer.write(2).is_ok());
        assert_eq!(Ok(1), buffer.read());
        assert!(buffer.write(-1).is_ok());
        assert_eq!(Ok(2), buffer.read());
        assert_eq!(Ok(-1), buffer.read());
        assert_eq!(Err(Error::EmptyBuffer), buffer.read());
    }

    #[test]
    fn string_buffer() {
        let mut buffer = CircularBuffer::new(2);
        buffer.write("".to_string()).unwrap();
        buffer.write("Testing".to_string()).unwrap();
        assert_eq!(0, buffer.read().unwrap().len());
        assert_eq!(Ok("Testing".to_string()), buffer.read());
    }
}
